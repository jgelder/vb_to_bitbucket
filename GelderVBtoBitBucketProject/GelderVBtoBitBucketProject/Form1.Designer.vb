﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnEnd = New System.Windows.Forms.Button()
        Me.nudInputOne = New System.Windows.Forms.NumericUpDown()
        CType(Me.nudInputOne, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnEnd
        '
        Me.btnEnd.Location = New System.Drawing.Point(339, 314)
        Me.btnEnd.Name = "btnEnd"
        Me.btnEnd.Size = New System.Drawing.Size(75, 23)
        Me.btnEnd.TabIndex = 0
        Me.btnEnd.Text = "Close"
        Me.btnEnd.UseVisualStyleBackColor = True
        '
        'nudInputOne
        '
        Me.nudInputOne.Location = New System.Drawing.Point(182, 134)
        Me.nudInputOne.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudInputOne.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudInputOne.Name = "nudInputOne"
        Me.nudInputOne.Size = New System.Drawing.Size(120, 22)
        Me.nudInputOne.TabIndex = 1
        Me.nudInputOne.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.nudInputOne)
        Me.Controls.Add(Me.btnEnd)
        Me.Name = "Form1"
        Me.Text = "VB to Bit Bucket Gelder"
        CType(Me.nudInputOne, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnEnd As Button
    Friend WithEvents nudInputOne As NumericUpDown
End Class
